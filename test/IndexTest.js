const sumar = require('../index');
const assert = require('assert');

//Asser = afirmacion

describe("Probar la suma de dos numeros", ()=>{
    //Afirmamos que 5+5=10
    it("Cinco mas cinco es 10", ()=>{
        assert.equal(10, sumar(5,5));
    });

    //Afirmamos que 5+7 no es 10
    it("Siete mas cinco no es 10", ()=>{
        assert.notEqual(10, sumar(7,5));
    });

});
{

}